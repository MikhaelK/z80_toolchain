@echo off
SetLocal EnableExtensions EnableDelayedExpansion
:: Указываем добавляемый каталог
set FolderToAdd0=C:\z80_toolchain\build\bin
set FolderToAdd1=C:\z80_toolchain\speccy
set FolderToAdd2=C:\z80_toolchain\tools
set FolderToAdd3=C:\z80_toolchain\z88dk\bin
:: Получаем текущее значение Path
set Key=HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
For /f "tokens=2*" %%a In ('Reg.exe query "%key%" /v Path^|Find "Path"') do set "CurPath=%%~b"
:: Дописываем новый каталог
reg.exe add "%Key%" /v Path /t REG_EXPAND_SZ /d "!CurPath!;!FolderToAdd0!" /F
:: Получаем текущее значение Path
set Key=HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
For /f "tokens=2*" %%a In ('Reg.exe query "%key%" /v Path^|Find "Path"') do set "CurPath=%%~b"
:: Дописываем новый каталог
reg.exe add "%Key%" /v Path /t REG_EXPAND_SZ /d "!CurPath!;!FolderToAdd1!" /F
:: Получаем текущее значение Path
set Key=HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
For /f "tokens=2*" %%a In ('Reg.exe query "%key%" /v Path^|Find "Path"') do set "CurPath=%%~b"
:: Дописываем новый каталог
reg.exe add "%Key%" /v Path /t REG_EXPAND_SZ /d "!CurPath!;!FolderToAdd2!" /F
:: Получаем текущее значение Path
set Key=HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
For /f "tokens=2*" %%a In ('Reg.exe query "%key%" /v Path^|Find "Path"') do set "CurPath=%%~b"
:: Дописываем новый каталог
reg.exe add "%Key%" /v Path /t REG_EXPAND_SZ /d "!CurPath!;!FolderToAdd3!" /F
:: Эта команда содержит API для обновления пользовательских настроек Path (только Vista+)
setx temp "%temp%"